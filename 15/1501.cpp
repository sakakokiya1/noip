#include <iostream>
using namespace std;
int main()
{
	//声明变量h表示纸张的厚度
	int h=1,n,cnt=0;//cnt表示对折的次数 
	cin >> n; //需要超过的厚度 30 
	//while循环  
	while(h<=n) 
	{
		//对折
		cnt++;  //1 2 3 4 5
		//加厚度
		h*=2;  //2 4 8 16 32
	}
	cout << cnt;
	return 0;
}


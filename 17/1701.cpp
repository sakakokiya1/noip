#include <iostream>
using namespace std;
int main()
{
	int m,n;
	cin >> m >> n;//m表示行,n是每行输出的第一个数字 
	// m,n == 3,5
	//外层循环控制行 
	for(int i=1;i<=m;i++)//1 2 
	{
		//内层循环控制列 
		for(int j=n;j>=1;j--) 
		{
			cout << j << " ";//5 4 3 2 1 
		}
		//换行 
		cout << endl; // 
	}
	return 0;
}

